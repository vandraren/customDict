import json
import typing as t


class CustomDict(t.Dict[str, t.Any]):
    def __init__(self, mapping_proxy, **kwargs: t.Any) -> None:
        super().__init__(**kwargs)
        self.classes = mapping_proxy

        for key, value in kwargs.items():
            attr = self.__get_attr(name=key, data=value)
            self.__setattr__(key, attr)
            self[key] = attr

    def __get_attr(self, *, name=None, data, class_=None):
        type_ = type(data)
        if type_ in [str, float, int]:
            return data
        else:
            if name:
                class_ = self.classes[name]
            elif not class_:
                raise ValueError('Neither name nor class was supplied as argument')

            return self.__process_object(type_, data, class_)

    def __process_object(self, type_, data, class_):
        if not isinstance(class_, type):
            return self.__process_collection(type_, class_, data)
        else:
            if issubclass(class_, CustomDict):
                return class_.init(**data)
            else:
                raise ValueError(f'{class_} not implemented')

    def __process_collection(self, type_, class_, data):
        typing_class = class_.__origin__
        if isinstance(typing_class, list.__class__):
            arg_pos = 0
        elif isinstance(typing_class, dict.__class__):
            arg_pos = 1
        else:
            raise ValueError(f'Collection {class_} not implemented')
        class_ = class_.__args__[arg_pos]

        if type_ == list:
            return [self.__get_attr(data=value, class_=class_) for value in data]
        elif type_ == dict:
            return {key: self.__get_attr(data=value, class_=class_) for key, value in data.items()}
        else:
            raise ValueError(f'{type_} not implemented')

    @classmethod
    def init(cls, **data):
        mapping_proxy = cls.__dict__['__annotations__']
        return cls(mapping_proxy, **data)

    def __hash__(self):
        return hash(json.dumps(self._dict(), sort_keys=True))

    NON_HASHABLE_ELEMENTS = ['__module__', '__annotations__', '__doc__', '__parameters__', 'classes']

    def _dict(self):
        dict_ = self.__dict__.copy()
        for attribute in CustomDict.NON_HASHABLE_ELEMENTS:
            if attribute in dict_:
                dict_.pop(attribute)
        return dict_

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return hash(self) == hash(other)
        return False

    def __str__(self):
        return str(self._dict())
