# CustomDict
CustomDict is a custom implementation of a subclass of the python dict type that allows for fully-fleshed objects to be spawned.

## How to install this package
You just need to add `customDict @ git+https://gitlab.com/vandraren/customDict@<tag/branch>` to your `requirements.txt` file.
