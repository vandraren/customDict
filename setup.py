from setuptools import setup

setup(
    name='customDict',
    version='v1.0.1',
    packages=['customDict'],
    url='https://gitlab.com/vandraren/customDict',
    license='GNU GPLv3',
    author='vandraren',
    author_email='tvandraren@gmail.com',
    description='A custom dict implementation',
    python_requires='>=3.8'
)
